package sahara.testapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class SelectDateActivity extends AppCompatActivity {

    Button btnDate,btnTime,btnConvert;
    TextView txtConvertedTime,txtConvertIntoLocalTime,txtFinalTimestamp;

    Calendar calendar;
    int year;
    int day;
    int month;

    int hour;
    int min;

    String dateAsString="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date);

        btnDate=findViewById(R.id.btnDate);
        btnTime=findViewById(R.id.btnTime);
        btnConvert=findViewById(R.id.btnConvert);
        txtConvertedTime=findViewById(R.id.txtConvertedTime);
        txtConvertIntoLocalTime=findViewById(R.id.txtConvertIntoLocalTime);
        txtFinalTimestamp=findViewById(R.id.txtFinalTimestamp);
        txtConvertIntoLocalTime.setVisibility(View.VISIBLE);
        txtConvertIntoLocalTime.setPaintFlags(txtConvertIntoLocalTime.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar=Calendar.getInstance();
                year=calendar.get(Calendar.YEAR);
                month=calendar.get(Calendar.MONTH);
                day=calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog=new DatePickerDialog(SelectDateActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        btnDate.setText(year + "-" + (month+1) + "-" +dayOfMonth);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        btnTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar=Calendar.getInstance();
                hour=calendar.get(Calendar.HOUR);
                min=calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog=new TimePickerDialog(SelectDateActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String AM_PM ;
                        if(hourOfDay < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        btnTime.setText(hourOfDay +":"+minute + " " + AM_PM);
                    }
                },hour,min,false);
                timePickerDialog.show();
            }
        });
        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnDate.getText().toString().equals("Select Date") &&
                        btnTime.getText().toString().equals("Select Time")){
                    Toast.makeText(SelectDateActivity.this, "Select Date & Time", Toast.LENGTH_SHORT).show();
                }else {
                    convert_into_utc(btnDate.getText().toString(),btnTime.getText().toString());
                }
            }
        });
        txtConvertIntoLocalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConvertIntoLocalTime(dateAsString);
            }
        });
    }

    private void convert_into_utc(String s1, String s2) {
        String dtStart = s1 + " "+s2;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");

        Date date = null;
        try {
            date = format.parse(dtStart);
            getDateInUTC(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void getDateInUTC(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateAsString = sdf.format(date);
        System.out.println("UTC" + dateAsString);
        txtConvertedTime.setText("Converted Time in UTC : "+dateAsString);
        txtConvertIntoLocalTime.setVisibility(View.VISIBLE);
    }

    private void ConvertIntoLocalTime(String dateAsString) {
        String dateStr = "2021-04-28T23:30:00.000Z";
//        String dateStr = dateAsString;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS'Z'", Locale.ENGLISH);
        SimpleDateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        String formattedDate="";
        try {
            date = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            formattedDate = df.format(date);
            String newDate = df1.format(date);
            txtFinalTimestamp.setText(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}