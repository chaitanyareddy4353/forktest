package sahara.testapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;

public class VideoScrollActivity extends AppCompatActivity {
//    List<VideoList>videoLists;
    ViewPager2 viewPager;
    VideoAdapter adapter;
    VideoView videoView;
    int timedur;
    private Handler handler;
    private int[] arrayvid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_scroll);


        arrayvid = new int[] { R.raw.raw1, R.raw.videoplayback, R.raw.raw1,
                R.raw.videoplayback, R.raw.raw1, R.raw.videoplayback, R.raw.videoplayback,
                R.raw.raw1, R.raw.videoplayback, R.raw.raw1 };



        viewPager=findViewById(R.id.viewPager);

        videoView=findViewById(R.id.videoView);

        //for full-screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);


        getdata();

    }

    private void getdata() {
        // Pass results to ViewPagerAdapter Class
        adapter = new VideoAdapter(VideoScrollActivity.this,viewPager, arrayvid);
        // Binds the Adapter to the ViewPager
        viewPager.setAdapter(adapter);

    }

}