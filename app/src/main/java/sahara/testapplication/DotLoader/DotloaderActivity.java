package sahara.testapplication.DotLoader;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import sahara.testapplication.R;

public class DotloaderActivity extends AppCompatActivity {

    DotLoader dotloader;
    Button btnStart;
    boolean isStart=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dotloader);

        dotloader=findViewById(R.id.dotloader);
//        dotloader.setVisibility(View.GONE);
        btnStart=findViewById(R.id.btnStart);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStart){
                    isStart=false;
                    dotloader.setVisibility(View.GONE);
                }else {
                    isStart=true;
                    dotloader.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}